# -*- encoding: utf-8 -*-
#
# A Caixa tem dois padrões para a geração de boleto: SIGCB e SICOB.
# Esta classe implementa o padrão SICOB para carteira Sem Registro
# http://downloads.caixa.gov.br/_arquivos/cobrcaixasicob/manuaissicob/ESPCODBARBLOQCOBRANREGIST_16POSICOES.pdf
#
module Brcobranca
  module Boleto
    class CaixaSicob < Base # Caixa Padrão Sicob

      # Validações
      validates_length_of :carteira, :is => 2, :message => 'deve possuir 2 dígitos.'
      validates_length_of :convenio, :is => 5, :message => 'deve possuir 4 dígitos.'

      # Nova instância da CaixaEconomica
      # @param (see Brcobranca::Boleto::Base#initialize)
      def initialize campos = {}
        campos.merge!(:convenio => campos[:convenio].rjust(5, '0')) if campos[:convenio]
        campos.merge!(:numero_documento => campos[:numero_documento].rjust(14, '0')) if campos[:numero_documento]
        campos.merge!(:local_pagamento => "PREFERENCIALMENTE NAS CASAS LOTÉRICAS ATÉ O VALOR LIMITE")

        super(campos)
      end

      # SR – Cobrança Sem Registro
      # @return [String]
      def carteira; 'SR' end

      # Código do banco emissor
      # @return [String]
      def banco; '104' end

      # Dígito verificador do código do banco em módulo 10
      # Módulo 10 de 104 é 0
      # @return [String]
      def banco_dv; '0' end

      # Composição: 8ZZZZZZZZZZZZZZ-D, Onde:
      # 8 - Constante
      # ZZZZZZZZZZZZZZ - Nosso Número do Cedente
      # D - *Dígito Verificador
      # @return [String]
      def nosso_numero_boleto
        "8#{numero_documento}-#{nosso_numero_dv}"
      end

      # Dígito verificador do Nosso Número
      # Dígito Verificador do Nosso Número calculado através do Modulo 11 (incluindo o 8)
      # @return [String]
      def nosso_numero_dv
        "8#{numero_documento}".modulo11_2to9_caixa.to_s
      end

      # Código do Cliente no sistema de Cobrança, informado pela CAIXA.
      # Formato AAAA.PPP.XXXXXXXX-DV, onde:
      # AAAA: Código da Agência do Cedente
      # PPP: Operação (870)
      # XXXXXXXX: Código do Cedente
      # DV: Dígito Verificador (Módulo 11)
      # @return [String]
      def agencia_conta_boleto
        "#{agencia}.870.#{convenio.rjust(8, '0')}-#{convenio_dv}"
      end

      # Dígito verificador do convênio ou código do cedente
      # Módulo de 11
      # @return [String]
      def convenio_dv
        "#{agencia}870#{convenio.rjust(8, '0')}".modulo11_2to9_caixa.to_s
      end

      # Monta a segunda parte do código de barras.
      # Código do Cliente Cedente Posição (Convênio)
      # Código da Agência
      # Constante 87
      # Nosso Número do Cliente Posição
      # @return [String]
      def codigo_barras_segunda_parte
        campo_livre = "#{convenio}" <<
        "#{agencia}" <<
        "87" <<
        "#{numero_documento}"

        "#{campo_livre}"
      end

    end
  end
end
